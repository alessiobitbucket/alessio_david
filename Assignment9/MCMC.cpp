#include <iostream>
#include <iomanip>
#include <string>
#include <map>
#include <random>
#include <cmath>
#include <fstream>
#include <ios>
#include <vector>

//Parameters in the code are highlighted: !!PARAMETER


////////////////////
////////////////////
////////////////////


//write on a file
void write ( std::vector<double>& x, double y, std::string file) {

	std::ofstream log(file, std::ios_base::app | std::ios_base::out); //set the output
	
	// write x[0]...x[N],f(X)
	int i;
	for (i=0; i<x.size(); i++) {

		log  << x[i] << " "; 
	}

	log  << y << "\n"; 
}


////////////////////
////////////////////
////////////////////


//return the mean of a vector
double mean ( std::vector<double>& a){ 

	double sum=0;
	int i;
	for (i=0; i<a.size(); i++) {
		sum += a[i];
	}

return sum/double(a.size());
}


////////////////////
////////////////////
////////////////////


//return the standard deviation of a vector
double standardvector( std::vector<double>& a){

	//The std is calculated on the difference between one element and the previous one
	int i;
	std::vector<double> diff( a.size()-1 );
	for (i=0; i<diff.size(); i++) {
		diff[i] = a[i+1]-a[i];
			
	}

	//build vector of squares
	std::vector<double> xx( diff.size() );
	for (i=0; i<diff.size(); i++) {
		xx[i] = pow( diff[i], 2);
			
	}

return sqrt( mean( xx ) - pow( mean( diff ), 2 ) );
}


////////////////////
////////////////////
////////////////////


//function to be sampled
double function( std::vector<double>& x, int& counterfunc){

	return pow(1-x[0],2)+100*pow((pow(x[0],2)-x[1]),2); //return the y
}


////////////////////
////////////////////
////////////////////


//function that gives proposal points
void normaldistr( std::vector< std::vector<double> >& x,  std::vector<double>& trialx, double counter ,double standard,  std::vector<double>& fix, std::string& file){ 

	//generate every coordinate of the proposed point
	int i;
	for (i=0; i<x.size(); i++) {

		// some stuff for the random generator
		std::random_device rd;
		std::mt19937 gen(rd());
		
	
		if ( counter < 2 ) { //a little push for the first points to move !!HERE PARAMETER
			fix[i] = 1; // !!HERE PARAMETER
			std::normal_distribution<double> nd( x[i][0] , fix[i] ); // set the generator
			trialx[i] = nd(gen); // get the new coordinate
	
		}else if ( counter > 1000 ) { // use last std if the burn-in is ended !!HERE PARAMETER
			std::normal_distribution<double> nd( x[i][0] , fix[i] ); // set the generator
			trialx[i] = nd(gen);// get the new coordinate
			file = "data.dat"; //change file to write data
	
		}else{ // proposal during the burn-in period
			fix[i] = standardvector(x[i]); // get std for i-th coordinate of 'some' last points
			std::normal_distribution<double> nd( x[i][0] , fix[i] ); // set the generator
			trialx[i] = nd(gen); // get the new coordinate

		}
	}
}


////////////////////
////////////////////
////////////////////


//return the covariance of two vectors
double covariance( std::vector< std::vector<double> >& x){
	
	int i;

	// The covariance is calculated on the difference between one element and the previous one
	std::vector<double> diff1( x[0].size()-1 );
	for (i=0; i<diff1.size(); i++) {
		diff1[i] = x[0][i+1]-x[0][i];		
	}
	
	std::vector<double> diff2( x[1].size()-1 );
	for (i=0; i<diff2.size(); i++) {
		diff2[i] = x[1][i+1]-x[1][i];		
	}

	// part of the algorithm to calculate the covariance
	std::vector<double> vec(diff1.size());
	for (i=0; i < diff1.size() ;i++) {
		vec[i] = diff1[i]*diff2[i];
	}

return mean(vec) - mean(diff1)*mean(diff2);
}


////////////////////
////////////////////
////////////////////


//generate bivariate normal distribution points
void multivariateND( std::vector< std::vector<double> >& x, std::vector<double>& trialx , std::vector<double>& SIGMA) {

	// some stuff for the random generator
	std::random_device rd;
	std::mt19937 gen(rd());
	
	//  algorithm to generate a point which is distributed as the covariance matrix given 
	std::normal_distribution<double> nd( 0.0 , 1.0 );

	double ro = SIGMA[2]/(SIGMA[0]*SIGMA[1]);
	double n0 = nd(gen);
	double n1 = nd(gen);
	
	trialx[0] = x[0][0]+SIGMA[0]*n0;
	trialx[1] = x[1][0]+SIGMA[1]*(ro*n0+pow(1-pow(ro,2),0.5)*n1);
}


////////////////////
////////////////////
////////////////////


// same concept of normaldistr(), but for the bivariate distribution
void normaldistr2( std::vector< std::vector<double> >& x,  std::vector<double>& trialx, double counter ,double standard, std::string& file, std::vector<double>& SIGMA ){

		if ( counter < 5 ) {

			SIGMA[0] = 10;
			SIGMA[1] = 10;
			SIGMA[2] = 0; 
			multivariateND( x , trialx, SIGMA ); 

		}else if ( counter > 1000 ) { 

			multivariateND( x,trialx, SIGMA);
			file = "data.dat";
		}else{
			SIGMA[0] = standardvector(x[0]);
			SIGMA[1] = standardvector(x[1]);
			SIGMA[2] = covariance(x);	
			multivariateND( x, trialx, SIGMA);
		}
}


////////////////////
////////////////////
////////////////////


//function to refresh the vectors once points are accepted
void refresh (std::vector< std::vector<double> >& x ,  std::vector<double>& y ,  std::vector<double>& trialx,  double trialy, int counter) {
				
	int i; // a parameter
	// refresh the vector of last points which stays always at fixed length 
	for (i=0; i<x.size() ; i++) {
		x[i].insert(x[i].begin(), trialx[i]); //insert new value in the vector
			if (counter>900) {x[i].pop_back();} //delete too old value !!HERE PARAMETER
	}

	// refresh the vector of last f(x) which stays always at fixed length (need it for debugging purpose)
	y.insert(y.begin(), trialy); //insert new value in the vector
	if (counter>5) {y.pop_back();}  // !!HERE PARAMETER (DEBUG)
}


////////////////////
////////////////////
////////////////////


//accept the point (refresh vectors and write a file	
void accept (std::vector< std::vector<double> >& x ,  std::vector<double>& y ,  std::vector<double>& trialx,  double trialy, int& counter, double& standard, std::string file) {

	counter++; // add one to the counter of points accepted
	
	write(trialx,trialy,file); // write the data
	refresh (x, y, trialx, trialy, counter); //refresh the vectors
			
	standard = standardvector( y ); //standard deviation last y's (need it for debugging purpose)
}


////////////////////
////////////////////
////////////////////


//function called for every starting point
void MCMC( std::vector<double>& start, int decision) {	
	
	std::vector< std::vector<double> > x(start.size(),std::vector<double>(1)); //vector of vector (inner vectors are the last N x_i components of the chain; so the outer vector store in every element the history of a component) 
	
	std::vector<double> y(1); //history of y (need it for debuggin purpose)
	std::vector<double> trialx(x.size()); //proposal x vector (a elemente for each component)

	double trialy; //proposal y 
	int counterfunc=0; //counter of evaluation of the function
	
	double standard; //standard deviation last y's (need it for debugging purpose)
	double alpha; //paramenter to accept/discard new proposals.
	double beta;  //paramenter to accept/discard new proposals.
	int counter=0; //count the number of points accepted
	std::vector<double> fix(x.size()); //store the std deviation for each component
	std::vector<double> SIGMA(3); // store the std of x1,x2 and covariance	
	std::string file = "burnin.dat"; //writing file
	int target=0; //a parameter for convergence 

	//initialise the starting point
	x[0][0]=start[0]; // !!PARAMETER
	x[1][0]=start[1]; // !!PARAMETER

	y[0]=function(start, counterfunc); //y(start point)
	write(start,y[0],file); //write in the file the start point
	
	while ( target<50 || counter < 1000) { // it ends if the last 50 f(x) are bigger then the previous (likely to be in the minimum) but only after the burn in period !!HERE PARAMETER

		//routine to generate the proposal points
		if (decision == 0) { //monovariate

			normaldistr( x, trialx, counter ,standard, fix, file);
		}else {  //bivariate

			normaldistr2( x, trialx, counter , standard, file, SIGMA );
			}

		trialy = function(trialx, counterfunc); // evaluate the function in the proposed point
		alpha = (trialy/y[0]); //alpha parameter			

		// loop of MCMC method to accept or reject points
		if (alpha >= 1.0) {
		
			target++; // add 1 to the convergence parameter
			beta = double(rand() %100) / 100.0;

			if (beta*alpha <= 1.0) {
				accept(x, y, trialx, trialy, counter, standard, file); // write the point and refresh the vectors
			}

		} else {accept(x, y, trialx, trialy, counter, standard, file);
			target=0; // set to zero the convergence parameter	
			}		
	}
}


////////////////////
////////////////////
////////////////////


int main() {

	//ask for mono or bivariate normal distribution
	int decision;
	std::cout<< "Type 0 for monovariate ND or 1 for bivariate ND ";
	std::cin>>decision;

	remove( "data.dat" ); //delete pre existent file if present
	remove( "burnin.dat" ); //delete pre existent file if present
	
	srand (time(NULL)); // seed the unform random genererator
	std::default_random_engine de(time(0)); // seed the normal distribution random number generator	
	
	int i,k; // indeces
	std::vector<double> start(2); // starting point.  Fix dimensionality
	
	for ( i=0; i<5; i++ ){ // it generates 5 starting points !!HERE PARAMETER

	std::ofstream log("burnin.dat", std::ios_base::app | std::ios_base::out); //first points writtein in burning.dat	
		
	for (k=0; k<start.size(); k++ ){
			start[k] = double(rand() %100) / 20.0 -2.5 ; //a random conteined in a box, for every dimension !!HERE PARAMETER
		}

	MCMC( start, decision ); // call the generale routine

	}

return 0;
}
