#include <iostream> // library to use "cout"
#include <cmath>  // include pow
#include <fstream> // library to write on .txt

double f1(double, double, double); // first ODE
double f2(double, double, double); // second ODE
void eval(double,double,double*,double*); // function that refresh the y values
