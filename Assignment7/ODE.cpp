#include "RK.h"

double f1(double x, double y1, double y2) { // first ODE
	return (-pow(y1,2)-y2);
}

double f2(double x, double y1, double y2) { // second ODE
	return 5*y1-y2;
}

