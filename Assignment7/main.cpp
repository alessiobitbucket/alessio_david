#include "RK.h"

int main () { // main function

// parameters
double min = 0; // startpoint
double max = 10; // endpoint
double acc = 0.000001; // relative error

double y1 = double(3)/2; // initial condition on y1
double y2 = double(3)/2; // initial condition on y2
double y1b, y2b, y1a, y2a; // explained in the loops

double x = min; // x value
double h; // step
double hold; // explained in the loops
double delta, delta1, delta2; // explained in the loops

std::ofstream myfile; // to write on .txt
myfile.open ("functions.txt"); // open .txt file

while (x<max) { // while loop to cover the interval 

	h = max-x; // the first trial step is the whole interval [x,max]
	if (max-x>1) {h=1;} // important condition because if x is far from max, the previous statement gets the code going crazy

	delta1 = acc+1; // guarantees to enter the loop
	delta2 = acc+1; // guarantees to enter the loop
	
	while ((delta1>acc) || (delta2>acc)) { // if the relative error of the first or the second equation is larger then the accuracy, it enters the loop
	
		//initialisation of the y values
		y1b = y1; // y1b is the y value of the first equation in the double step case
		y2b = y2; // y2b is the y value of the second equation in the double step case
		y1a = y1; // y1a is the y value of the first equation in the single step case
		y2a = y2; // y2a is the y value of the second equation in the single step case

		eval( x, h, &y1a, &y2a); // refresh y1a and y2a

		h = h/2; // step for the double step case
		eval( x, h, &y1b, &y2b); // refresh y1b and y2b with half step
		eval( x+h, h, &y1b, &y2b); // refresh y1b and y2b with the other half step
		
		delta1 = std::abs(( y1a-y1b )/y1a); // relative error of the first equation
		delta2 = std::abs(( y2a-y2b )/y2a); // relative error of the second equation
 
		// take the larger error between the two
		if (delta1>delta2){ 
			delta=delta1;} else {delta=delta2;}

		hold = 2*h; // recovers the value of the step of the single step case

		h = 2*h*pow(acc/delta,double(1)/5); // redifine the step

	}
	
	// since it quit the loop, the y values are assumed to be enough accurate
	y1=y1a; // refresh initial value of the first equation 
	y2=y2a; // refresh initial value of the second equation 

	myfile << x <<" "<< y1 <<" "<< y2 << "\n"; // write on .txt file

	x = x+hold; // moves along x axis

}
		
return 0;

}
