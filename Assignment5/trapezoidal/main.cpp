#include "trapez.h"

int main (void) { // main function

	double L; // left extremity
	std::cout << "Insert the left interval followed by enter: "; // ask to enter the left extremity
	std::cin >> L; // enter the left extremity

	double R; // right extremity	
	std::cout << "Insert the right interval followed by enter: "; // ask to enter the right extremity
	std::cin >> R; // enter the right extremity

	double eps; // accuracy
	std::cout << "Insert the accuracy (less than 1) followed by enter: "; // ask to enter the accuracy
	std::cin >> eps; // enter the accuracy
	
	double In=0;
	double k=0;
	trapez(L, R, eps, In, k); //call the function that calculate the integral

	std::cout << "The value of the integral is: " << In << std::endl; //declare the value of the integral
	std::cout << "The number of iterations is: " << k + 1 << std::endl; //declare the number of iterations (k+2 because the first iteration is outside the loop and the loop start with k=0, k+2-1=0 because at the end of the loop it adds 1)
}
