#include "trapez.h"

void trapez (double L,double R, double eps, double& In,double& k ) {
	
	double A = R-L; // first interval 
	In = A*0.5*(f(L)+f(R)); // first value of the integral (integral new)
	double Io = In/(eps+1)+1; // an arbitrary previous integral that I know for sure that will allow to enter the next while loop if accuracy < 1 is set

	int i=0; // a parameter 
	double B; // I will talk about it in the while loop
	double F; // I will talk about it in the while loop
	double T; // I will talk about it in the while loop

	while ( std::abs((Io-In)/Io) >= eps ) { // the programme enter in the while loop if the relative error is larger than the accuracy
	
		Io = In; // the old integral is the new integral (every loop new and old are redifined)
		B = A/2; // the new interval
		F = B*(Io/A); // is the part of the old integral that divided by the old interval and mutiplied by the new interval can be added to the function calculated in the new points
		T=0; // the new addend of the integral which will be created in the for loop

		//for loop to create the new addend of the trapeizoidal rule
		for (i=0 ; i<=pow(2,k)-1 ; i++) { // the number of point for every iteration goes like powers of 2. -1 because the last one would be larger than the right extremity
			T = T+B*f(L+B+i*A); // the new addend of the rule which is repetetevely ridefined as the function is calculated in every point (that is spaced from the other new points as the old interval)
		}

		In = T+F; // integral new is composed by the old integral renormalised F and the new part T
		A=B; // the new interval gets old for the next loop
		k++; // k increase for the next while loop

		// to prevent exceeding of calculation time
		if (k>24) { // 24 is an arbitrary k which take about 10s (by the way imply an accuracy far small than E-6)
			std::cout << "Computation time excedeed with accuracy " << std::abs((Io-In)/Io) << std::endl; // sais that it takes too long and delcare the accuracy with whom the integral has been computed
			break; // break the while loop;
		}
	}
}
