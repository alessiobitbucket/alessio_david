#include "trapez.h"

double e = 2.7182818284;
double pi = 3.1415926535;

double f(double x) {
	return 2/sqrt(pi)*pow(e,-pow(x,2));
}