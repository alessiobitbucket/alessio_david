//I comment the lines which are different from trapez.cpp

#include "simpson.h"

double simpson (double L,double R, double eps, double& In,double& k ) {

	double A = R-L; 
	double T = A*0.5*(f(L)+f(R)); // Part of the integral calculated with the trapeizoidal rule
	double M = A*f((f(L)+f(R))/2); // Part of the integral calculated with the mean rule
	In = (T+2*M)/3;
	double Io = In/(eps+1)+1;
	int i=0;
	double B;
	double F;
	double U; // I will talk about it in the while loop

	while (std::abs((Io-In)/Io) >= eps) {
		
		Io = In;
		B = A/2;
		F=B*(T/A);
		U=0; // the new addend of the integral which will be created in the for loop (part of the integral given by the mean rule)
		M=0;

		for (i=0 ; i<=pow(2,k)-1 ; i++) {
			U = U+B*f(L+B+i*A);
			M = M+B*f(L+B/2+i*B); 
		}
		
		//the number of point for every cycle is 2^k-1 for the trapeizoidal part and 2^(k+1)-1 for the mean rule part. Here I complete the loop for the mean part
		for (i=i++ ; i<=pow(2,k+1)-1 ; i++) {
			M = M+B*f(L+B/2+i*B);
		}

		T = U+F; // trapeizoidal part of the integral (the mean part is redifined completely every while loop, because the points have changed. So there is no sum for the "mean rule" pary)
		In = (T+2*M)/3; //simpson rule obtained by the sum of trapeizoidal and "mean rule" integral parts
		A=B;
		k++;

		if (k>24) {
			std::cout << "Computation time excedeed with accuracy " << std::abs((Io-In)/Io) << std::endl;
			break;
		}
	
	}
}