#include <iostream> //library for streaming messages
#include <boost/random/mersenne_twister.hpp> //library to use mersenne twister random numbers generator
#include <cmath> //library to use pow
#include <fstream> //library to write on .txt
#include <time.h> //library to show the time

int main(void){ //main function

	// t is the time at the start of the programme
	clock_t t;
	t = clock();

	boost::mt19937 gen2(999);//seed the generator with 999
	
	std::ofstream myfile; // to write on .txt
	myfile.open ("sin2.txt"); // open .txt file
	
	const size_t n=100000;  //tells the generator to produce 10^5 random numbers
	double pi = 3.14159265359; //register pi value

	// loop that generates every data which is distributed as sin^2
	for(size_t i=0; i<n; ++i) {
		double x = acos(-double(gen2())/pow(2,31)+1); // x value for which integral{2/pi*[sin(x)]}= random number [0,4/pi]
		double y = double(gen2())/pow(2,31)/pi*sin(x); // y random value within 0 and 2/pi*sin(x) 
		double z = 2/pi*pow(sin(x),2); // y value of sin^2(x)
		
		//for loop to print in a .txt only the x values whose y is less than z
		if (y<z) {
		myfile << x << "\n";
		} else {}
	}
	
	std::cout << "This process took " <<double(clock()-t)/CLOCKS_PER_SEC << " s" << std::endl; // time needed for the programme  

}
