#include <iostream> //library for streaming messages
#include <boost/random/mersenne_twister.hpp> //library to use mersenne twister random numbers generator
#include <cmath> //library to use pow
#include <fstream> //library to write on .txt

int main(void){ //main function
	
	boost::mt19937 gen2(999); //seed the generator with 999
	
	std::ofstream myfile; // to write on .txt
	myfile.open ("uniform.txt"); // open .txt file
	
	const size_t n=100000; //tells the generator to produce 10^5 random numbers
	
	//loop to write every random number in the .txt
	for(size_t i=0; i<n; ++i) { 
		myfile << gen2()/pow(2,32) <<"\n"; // output is normalised [0,1]
	}
}
