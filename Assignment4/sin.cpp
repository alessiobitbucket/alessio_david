#include <iostream> //library for streaming messages
#include <boost/random/mersenne_twister.hpp> //library to use mersenne twister random numbers generator
#include <cmath> //library to use pow
#include <fstream> //library to write on .txt
#include <time.h> //library to show the time

int main(void) { //main function

	// t is the time at the start of the programme
	clock_t t;
	t = clock();

	boost::mt19937 gen2(999); //seed the generator with 999
	
	std::ofstream myfile; // to write on .txt
	myfile.open ("sin.txt"); // open .txt file

	const size_t n=100000; //tells the generator to produce 10^5 random numbers
	
	//for loop to print the values distributed as sin(x)
	for(size_t i=0; i<n; ++i) {
		myfile << acos(double(gen2())/pow(2,31)-1) <<"\n"; // arccos of randoms [-1,1], which is linked to the distribution 1/2*sin(x)
	}
	
	std::cout << "This process took " <<double(clock()-t)/CLOCKS_PER_SEC << " s" << std::endl; // time needed for the programme  
}
