#include "MC.h"

int main(void) {

		
	std::srand(std::time(0)); // feed rand() with time
	

	std::ofstream myfile; // to write on .txt
	myfile.open ("data.txt"); // open .txt file in which writing the value of the integral and accuracy at every iteration
	
	double err; // precision to enter
	std::cout<< "Insert precision: "; // ask for precision
	std::cin>> err; // store precision

	int decision; // a variable to decide which type of importance sampling to use
	std::cout<< "Which sampling do you want to choose? \n type 1 for constant \n type 2 for linear: ";// ask for the sampling to use
	std::cin>> decision; // store the decision

//	1D case
	void (*sampl1)(double *); // initialising a pointer to the function which generate randoms
	double (*sampl2)(double); // initialising a pointer to the pdf (importance distribution)
		
/*	2D case:
	void (*sampl1)(double *,double *); // initialising a pointer to the function which generate randoms
	double (*sampl2)(double,double); // initialising a pointer to the pdf (importance distribution)
*/

	if (decision==1){ // if loop to dereference in case of costant sampling
		sampl1 = constant1;
		sampl2 = constant2;
	}

	else if (decision==2){  // if loop to dereference in case of linear sampling
		sampl1 = linear1;
		sampl2 = linear2;
	}

	else {abort();} // end run if you type something else then 1,2
	
	double n = 50; // initial number of randoms
	double In; // new value for the integral
	double Io; // old value for the integral
	double eps = err+1; // error of integration, initialised larger then the precision entered in order to enter the loop
	int i; // a parameter

	double time = std::clock(); // time before iterations

	while (err<eps && (std::clock()-time)/CLOCKS_PER_SEC < 10) { //check if the precision entered is larger then the error in the calculation and if the time exceeds 10s

		Io=In; // the new value of the integral become the old value for the starting loop
		In=0; // Set to zero to be recalculated in the next loop

		for (i=0 ; i<=n ; i++) { // calculation of the integral with a sum of n terms
	
			//1D case
			double x; // point in which is calculated the integral
			sampl1(&x); // generates a point distributed as the sampling
			
		/*	2D case
			double x;
			double y;
			sampl1(&x,&y);
		*/

			In =  In + f(x)/sampl2(x); // MC integration formula to generate the integral
 
		//	In =  In + f(x,y)/sampl2(x,y); for the 2D case	
	
		}
	
		In = In / n; // MC integration formula (second part)
	
		eps = std::abs((In-Io)/In); // accuracy

		myfile  << eps << " " << In << "\n"; // write the integral and the accuracy in a .txt	

		n=2*n; // double the number of randoms for the next loop

	}

	if ((std::clock()-time)/CLOCKS_PER_SEC > 10) {
	std::cout<< "Time excedeed. The value of the integral is: " << 0.5*(In+Io) <<" with accuracy "<< eps << std::endl; // tell the value of the integral if time-out occours
	}
	else {std::cout<< "The value of the integral is: " << 0.5*(In+Io) <<std::endl; // tell the value of the integral
	}
}
