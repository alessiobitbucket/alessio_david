#include <iostream> //library for streaming messages
#include <cmath> //library to use pow
#include <fstream> //library to write on .txt
#include <cstdlib> // to use std::rand()
#include <ctime> // to use time to seed rand()

double f (double);
void linear1(double*); //void linear1(double*,double*) for the 2D case
void constant1(double*); //void constant1(double*,double*) for the 2D case
double linear2(double); //double linear2(double,double) for the 2D case 
double constant2(double); //double constant2(double,double) for the 2D case 


