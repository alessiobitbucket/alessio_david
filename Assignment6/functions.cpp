#include "MC.h"


//integrand
double f(double x) { //double f(double x,double y) for 2D case

	double e = 2.7182818284;
	double pi = 3.1415926535;

return 2/sqrt(pi)*pow(e,-pow(x,2));

}


// generate a linear distribution of randoms
void linear1(double*x) { // void linear1( double*x , double*y ) for 2D case

	double A = 0.48;
	double B = 0.98;
	*x = (B-sqrt(pow(B,2) - 2*A*double(std::rand()/2.14748e+09 )))/A ;
}


// generate a costant distribution of randoms
void constant1(double*x) { // void constant1( double*x , double*y ) for 2D case


	double C = 0.5;	
	*x =std::rand()/2.14748e+09/C;
}
		


// pdf linear distribution
double linear2(double x ) { //linear2(double x, double y) for the 2D case
	
	double A = 0.48;
	double B = 0.98;
	double y = -A*x+B;

return y;

}



//pdf costant distribution
double constant2(double x) { //constant2(double x, double y) for the 2D case
	
	double C = 0.5;
	double y = C;

return y;

}
