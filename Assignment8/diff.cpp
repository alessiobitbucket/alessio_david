#include "RK.h"

double diff(double y2) {

// parameters
double min = 0; // startpoint
double max = 10; // endpoint
double acc = 0.000001; // relative error

double y1 = double(3)/2; // initial condition on y1
double y1fin = 0.0; // final value of y1
double y1b, y2b, y1a, y2a; // explained in the loops

double x = min; // x value
double h; // step
double hold; // explained in the loops
double delta, delta1, delta2; // explained in the loops

std::ofstream myfile; // to write on .txt
myfile.open ("functions.txt"); // open .txt file
myfile << x <<" "<< y1 <<" "<< y2 << "\n"; // write on .txt file the first values


while (x<max) { // while loop to cover the interval

		if (max-x>1) {h=1;}else{h = max-x; // the first trial step is the whole interval [x,max]
		} // important condition because if x is far from max, the previous statement gets the code going crazy
//std::cout<<"hprima"<< h <<std::endl;

	delta1 = acc+1; // guarantees to enter the loop
	delta2 = acc+1; // guarantees to enter the loop
	
	while ((delta1>acc) || (delta2>acc)) { // if the relative error of the first or the second equation is larger then the accuracy, it enters the loop
	
		//initialisation of the y values
		y1b = y1; // y1b is the y value of the first equation in the double step case
		y2b = y2; // y2b is the y value of the second equation in the double step case
		y1a = y1; // y1a is the y value of the first equation in the single step case
		y2a = y2; // y2a is the y value of the second equation in the single step case

		eval( x, h, &y1a, &y2a); // refresh y1a and y2a

		eval( x, h/2.0 , &y1b, &y2b); // refresh y1b and y2b with half step
		eval( x+h/2.0, h/2.0 , &y1b, &y2b); // refresh y1b and y2b with the other half step
		
		delta1 = std::abs(( y1a-y1b )/y1a); // relative error of the first equation
		delta2 = std::abs(( y2a-y2b )/y2a); // relative error of the second equation
		
		// take the larger error between the two
		if (delta1>=delta2){ 
			delta=delta1;} else {delta=delta2;}

		hold = h; // re-take the value of the step of the single step case

		h = h*pow(acc/delta,double(1)/5); // redifine the step

	}
	
	// since it quit the loop, the y values are assumed to be enough accurate
	y1=y1b+(delta1*y1a)/15; // refresh initial value of the first equation 
	y2=y2a+(delta2*y2a)/15; // refresh initial value of the second equation 

	myfile << x <<" "<< y1 <<" "<< y2 << "\n"; // write on .txt file
	x = x+hold; // moves along x axis

}


return y1-y1fin; // return the value of y1-y1(right) for a y2(0) guess

}
