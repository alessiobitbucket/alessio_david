#include "RK.h"

// RK routine for the fist and second equation
// I pass the x value in the ODEs for generality (in this case they don't depend explicity in x)
void eval(double x, double h ,double* y1, double* y2) {

	double k1a,k1b,k2a,k2b,k3a,k3b,k4a,k4b;

	k1a=h*f1(x, *y1, *y2);
	k1b=h*f2(x, *y1, *y2);

	k2a=h*f1(x+h/2, *y1+k1a/2, *y2+k1b/2);
	k2b=h*f2(x+h/2, *y1+k1a/2, *y2+k1b/2);

	k3a=h*f1(x+h/2, *y1+k2a/2, *y2+k2b/2);
	k3b=h*f2(x+h/2, *y1+k2a/2, *y2+k2b/2);

	k4a=h*f1(x+h, *y1+k3a, *y2+k3b);
	k4b=h*f2(x+h, *y1+k3a, *y2+k3b); 	
 
	// refresh y1 and y2
	*y1=*y1+double(1)/6*k1a+double(1)/3*k2a+double(1)/3*k3a+double(1)/6*k4a;
	*y2=*y2+double(1)/6*k1b+double(1)/3*k2b+double(1)/3*k3b+double(1)/6*k4b;
}

