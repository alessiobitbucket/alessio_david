void solve(double* a, double* b, double* c, double* f, int n) {
    
    n--;
    c[0] /= b[0];
    f[0] /= b[0];

    for (int i = 1; i < n; i++) {
	c[i] /= b[i] - a[i]*c[i-1];
        f[i] = (f[i] - a[i]*f[i-1]) / (b[i] - a[i]*c[i-1]);
    }

    f[n] = (f[n] - a[n]*f[n-1]) / (b[n] - a[n]*c[n-1]);

    for (int i = n; i-- > 0;) {
        f[i] -= c[i]*f[i+1];
    }
}