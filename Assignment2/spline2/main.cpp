#include <iostream>
#include <cmath>
#include <string>
#include "spline.h"
#include <stdexcept>
#include <fstream>
using namespace std;

int main () { //main function
	
	int N=11;
	N--;
	
	double x[11] = {-2.1,-1.45,-1.3,-0.2,0.1,0.15,0.8,1.1,1.5,2.8,3.8};
	double y[11] = {0.012155,0.122151,0.184520,0.960789,0.990050,0.977751,0.527292,0.298197,0.105399,0.0003936690,0.0000005355348};
	int i;

	int nat;	
	cout << "Do you want to interpolate a natural spline or a definite first derivative spline? Type 1 or 2 :";
	cin >> nat;
	
	//vectors of tridiag matrix	
	double b[N];
	double a[N];
	double c[N];
	double f[N];

	if (nat==1){ //do the natural interpolation

		//diagonal vector
		b[0] = 1;
		b[N] = 1;
		for (i=1 ; i<=(N-1) ; i++){
			b[i] = (x[i+1]-x[i-1])/double(3);// ricordarsi di cambiarlo anche in spline.1 e anche sotto
			}

		//lowerdiag vector
		a[0] = 0; //zero by definition
		a[N] = 0; //zero if it's natural
		for (i=1 ; i<=(N-1) ; i++){
			a[i] = (x[i]-x[i-1])/double(6);
			}

		//upperdiag vector
		c[N] = 0; //zero by definition
		c[0] = 0; //zero if it's natural
		for (i=1 ; i<=(N-1) ; i++){
			c[i] = (x[i+1]-x[i])/double(6);
			}

		//parameters vector
		f[0] = 0;
		f[N] = 0;
		for (i=1 ; i<=(N-1) ; i++){
			f[i] = (y[i+1]-y[i])/(x[i+1]-x[i])-(y[i]-y[i-1])/(x[i]-x[i-1]);
			}
	}

	if (nat==2) { //do the set derivative interpolation
		double der1;
		double der2;
		cout << "Specify the value of the first derivative at the starting point: ";
		cin >> der1;
		cout << "Specify the value of the first derivative at the ending point: ";
		cin >> der2;
		
		//diagonal vector
		b[0] = (x[1]-x[0])/double(3); //non so se l'ho cambiato sul grafico
		b[N] = (x[N-1]-x[N])/double(3); //non so se l'ho cambiato sul grafico
		for (i=1 ; i<=(N-1) ; i++){
			b[i] = (x[i+1]-x[i-1])/double(3);
			}

		//lowerdiag vector
		a[0] = 0; //zero by definition
		a[N] = (x[N-1]-x[N])/double(6); //zero if it's natural
		for (i=1 ; i<=(N-1) ; i++){
			a[i] = (x[i]-x[i-1])/double(6);
			}

		//upperdiag vector
		c[N] = 0; //zero by definition
		c[0] = (x[1]-x[0])/double(6); //zero if it's natural
		for (i=1 ; i<=(N-1) ; i++){
			c[i] = (x[i+1]-x[i])/double(6);
			}

		//parameters vector
		f[0] = (y[1]-y[0])/(x[1]-x[0])-der1;
		f[N] = (y[N]-y[N-1])/(x[N]-x[N-1])-der2;
		for (i=1 ; i<=(N-1) ; i++){
			f[i] = (y[i+1]-y[i])/(x[i+1]-x[i])-(y[i]-y[i-1])/(x[i]-x[i-1]);
			}
	}
	
	
	//solving the matrix system
	solve(a,b,c,f,N);
	cout<< f[0]<<f[1]<<f[2]<<f[3]  <<endl;

	
	double A; //controlare cos ho messo in spline 1
	double B;
	double C;
	double D;
	
	double X[100];
	double Y[100];
	
	int j;	
	
	for (j=0 ; j<100 ; j++) {

		X[j] = (x[N]-x[0])*double((j+1))/double(101)+x[0]; //generate 20 equispaced points .c'e' un problema se l'intervallo corrisponde con il punto
		
		//detect in which interval you are
		i = 0;
		while (X[j] > x[i]) {
			i = i+1;
		}
		i--; //where to build the equation for Y

		A = (x[i+1]-X[j])/(x[i+1]-x[i]);
		B = 1-A;
		C = double(1)/double(6)*(pow(A,3)-A)*pow((x[i+1]-x[i]),2);
		D = double(1)/double(6)*(pow(B,3)-B)*pow((x[i+1]-x[i]),2);

		Y[j] =  A*y[i] + B*y[i+1] + C*f[i] + D*f[i+1];
	}
		
	//write in the file
	ofstream myfile;
	myfile.open ("spline.txt");
	for (i=0 ; i<=99 ; i++) {
		myfile << X[i]<<","<<Y[i]<<"\n";
	}

return 0;
}
	