#include <iostream>
#include <cmath>
#include <string>
#include "spline.h"
#include <stdexcept>
#include <fstream>
using namespace std;

int main () { //main function
	
	int N=11; //number of "sperimental" points 
	double x[11] = {-2.1,-1.45,-1.3,-0.2,0.1,0.15,0.8,1.1,1.5,2.8,3.8};
	double y[11] = {0.012155,0.122151,0.184520,0.960789,0.990050,0.977751,0.527292,0.298197,0.105399,0.0003936690,0.0000005355348};
	int i; //an index
	
	//this paragrap is for deciding to do natural spline or first derivative set interpolation
	int option; //a value for the decision
	cout << "Do you want to interpolate a natural spline or a definite first derivative spline? Type 1 for the first option or 2 for the other one and press enter:";
	cin >> option; // assign the value for the decision
	
	//vectors of the tridiagonal matrix	
	double b[N];
	double a[N];
	double c[N];
	double f[N];

	//this "if" paragraph do the natural interpolation
	if (option==1){

		//diagonal vector
		b[0] = 1;
		b[N] = 1;
		for (i=1 ; i<=N-1 ; i++){
			b[i] = (x[i+1]-x[i-1])/3;
			}

		//subdiagonal vector
		a[0] = 0; //zero by definition (this is related to the form of solver I used)
		a[N] = 0; //zero because it's natural
		for (i=1 ; i<=N-1 ; i++){
			a[i] = (x[i]-x[i-1])/6;
			}

		//superdiagonal vector
		c[N] = 0; //zero by definition
		c[0] = 0; //zero because it's natural
		for (i=1 ; i<=N-1 ; i++){
			c[i] = (x[i+1]-x[i])/6;
			}

		//known values vector
		f[0] = 0;
		f[N] = 0;
		for (i=1 ; i<=N-1 ; i++){
			f[i] = (y[i+1]-y[i])/(x[i+1]-x[i])-(y[i]-y[i-1])/(x[i]-x[i-1]);
			}
	}

	//this "if" paragraph do the first derivate set interpolation
	if (option==2) {
		
		double der1; //first derivative at startpoint
		double der2; //first derivative at endpoint
		cout << "Specify the value of the first derivative at the starting point and press enter: ";
		cin >> der1; //set the first derivative at startpoint
		cout << "Specify the value of the first derivative at the ending point and press enter: ";
		cin >> der2; //set the first derivative at endpoint
		
		//diagonal vector
		b[0] = (x[1]-x[0])/3; //non so se l'ho cambiato sul grafico
		b[N] = (x[N-1]-x[N])/3; //non so se l'ho cambiato sul grafico
		for (i=1 ; i<=N-1 ; i++){
			b[i] = (x[i+1]-x[i-1])/3;
			}

		//subdiagonal vector
		a[0] = 0; //zero by definition (this is related to the form of solver I used)
		a[N] = (x[N-1]-x[N])/6;
		for (i=1 ; i<=N-1 ; i++){
			a[i] = (x[i]-x[i-1])/6;
			}

		//superdiagonal vector
		c[N] = 0; //zero by definition (this is related to the form of solver I used)
		c[0] = (x[1]-x[0])/6;
		for (i=1 ; i<=N-1 ; i++){
			c[i] = (x[i+1]-x[i])/6;
			}

		//parameters vector
		f[0] = (y[1]-y[0])/(x[1]-x[0])-der1;
		f[N] = (y[N]-y[N-1])/(x[N]-x[N-1])-der2;
		for (i=1 ; i<=N-1 ; i++){
			f[i] = (y[i+1]-y[i])/(x[i+1]-x[i])-(y[i]-y[i-1])/(x[i]-x[i-1]);
			}
	}
	
	
	//solving the matrix system
	solve(a,b,c,f,N);
	
	int j;
	double X[4] = {0.4,-0.128,-2.0,3.2};
	double Y[4];
	for (j=0 ; j<4 ; j++) {	
		
		i = 0;
		while (X[j] > x[i]) {
			i = i+1;
		}
		i--; //this is the index of the lower x closest to X

		//build the equation of the interval in which the X point is
		double A;
		double B;
		double C;
		double D;

		A = (x[i+1]-X[j])/(x[i+1]-x[i]);
		B = 1-A;
		C = double(1)/double(6)*(pow(A,3)-A)*pow((x[i+1]-x[i]),2);
		D = double(1)/double(6)*(pow(B,3)-B)*pow((x[i+1]-x[i]),2);
		Y[j] =  A*y[i] + B*y[i+1] + C*f[i] + D*f[i+1];
	}
		//write in the file
		ofstream myfile;
		myfile.open ("spline.txt");
		for (i=0 ; i<=3 ; i++) {
			myfile << X[i]<<","<<Y[i]<<"\n";
			}
	
	cout << "END" << endl;
return 0;
}
	